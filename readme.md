# Action-Monitor app

You will be required to have installed : 

- Java 1.8
- Maven 3

In order to run the application locally, just follow the following steps:
  
- Clone the repository
- Locate into the repository folder
- Checkout to the develop branch
- mvn spring-boot:run

#### Important
It exists a file inside the application source code (application.yml inside config folder) which is intended to be for development propurses. This file specify things like the port in which the application will be running. Althoug unlikely, you may have to change some of their values in case for example you have the port defined there bound to another process.

## Unit Testing

In my case, I prefer to follow classical unit testing strategy when possible, but sometime is difficult due to complexity of the classes interactions and also because of the amount of time required to implement them. Despite this, they allow us to test the state of the system after a real execution of the method under test, and does not couple us to the implementation at all. They have some dissadvantages, for example when a method has a side effect (like a DB update) we are not allow to check that tha behavoir is triggered. 

What I meant is that we cannot and we must not just take one path or the other, but choose any, depending on the circusntances. I obviously used both for this POC.

## How to

I am going to define briefly the different section in action monitor app. 

Assuming that the values of the properties in the application.yml file are the one I set we can find the following endpoints:

- http://localhost:8080/info : Info Endpoint which will provide application information. Implemente with spring actuator
- http://localhost:8080/health : Health Endpoint, which will tell us whether the application is up or not. (Default implementation of spring actuator).
- http://localhost:8080/#!/action-monitor : Action monitor, where We will log all the actions that take place in our system.
- http://localhost:8080/#!/login : Login Stage, Simple form to enter a username. 
- http://localhost:8080/#!/communication: Communication stage, where the user logged in our system will be able to send/receive messages. We will have on the left side a form which will allow us to send a message by adding to whom we want to send the message and the message itself. In the right side we will see the messages we have seent. The user will be allowed to edit the message sent by just clicking on the message and edit the text in the dialog that will appear. We will be receiving messages and they will appear on the central side of the page.
- http://localhost:8080/h2-console : In case you need/want to access the DB we use for this POC you could do it through this endpoint. 
  - Driver class : org.h2.Driver
  - JDBC URL : jdbc:h2:mem:test;MODE=MySQL
  - User Name : sa