
var app = angular
	.module('actionMonitorApp',['ui.router','ngAnimate','ngAria','ngMaterial'])
	.config(routerConfig);;



function routerConfig($stateProvider, $urlRouterProvider) {

	$stateProvider
	.state('action-monitor', {
		url: '/action-monitor',
		templateUrl: 'js/app/action-monitor/action-monitor.html',
		controller: 'ActionMonitorController',
		controllerAs: 'actionMonitorController'
	})
	.state('login', {
		url: '/login',
		templateUrl: 'js/app/login/login.html',
		controller: 'LoginController',
		controllerAs: 'loginController'
	})
	.state('communication', {
		url: '/communication',
		templateUrl: 'js/app/communication/communication.html',
		controller: 'CommunicationController',
		controllerAs: 'communicationController'
	});

	$urlRouterProvider.otherwise('/login');
}