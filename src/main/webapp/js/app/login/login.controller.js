(function() {
	'use strict';

	angular
	.module('actionMonitorApp')
	.controller('LoginController', LoginController);

	/** @ngInject */
	function LoginController($state,ConnectionConfig) {
		var vm = this;
		vm.username = null;
		ConnectionConfig.stompClient = null;
		
		vm.loginUser = function(loginForm){
			if(vm.username){				
				ConnectionConfig.socket = new SockJS('/ws');
				ConnectionConfig.stompClient = Stomp.over(ConnectionConfig.socket);
				ConnectionConfig.username = vm.username;
				$state.go("communication")
	
			}
		}
	}
})();
