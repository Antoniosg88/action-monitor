(function() {
	'use strict';

	angular
	.module('actionMonitorApp')
	.controller('ActionMonitorController', ActionMonitorController);

	/** @ngInject */
	function ActionMonitorController($scope, ConnectionConfig) {
		
		var vm = this;
		vm.messagesRecieved = new Array();
	
		vm.onMessageReceived = function(payload){
			vm.messagesRecieved.push(JSON.parse(payload.body));
			$scope.$apply();
		}
		
		vm.onConnectedCallback = function(){
			ConnectionConfig.stompClient.subscribe('/channel/action-monitor', vm.onMessageReceived);
		};

		vm.onErrorCallback = function(){
			$state.go("login");
		}
		ConnectionConfig.socket = new SockJS('/ws');
		ConnectionConfig.stompClient = Stomp.over(ConnectionConfig.socket);

		ConnectionConfig.stompClient.connect({},vm.onConnectedCallback,vm.onErrorCallback);
		
	}
	
})();