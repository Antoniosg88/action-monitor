(function() {
	'use strict';

	angular
	.module('actionMonitorApp')
	.controller('CommunicationController', CommunicationController);

	/** @ngInject */
	function CommunicationController($state,$scope,$filter,$mdDialog, ConnectionConfig) {
		var vm = this;
		vm.messagesRecieved = new Array();
		vm.messagesSent = new Array();
		
		vm.onErrorCallback = function(){
			$state.go("login");
		}
		
		vm.onConnectedCallback = function(){
			ConnectionConfig.stompClient.subscribe('/channel/'+ConnectionConfig.username, vm.onMessageReceived);
		};
		
		if(ConnectionConfig.username == null){
			$state.go("login");
		} else {
			ConnectionConfig.stompClient.connect({username:ConnectionConfig.username},vm.onConnectedCallback,vm.onErrorCallback);
		}
		vm.onMessageReceived =function(payload){
			var message = JSON.parse(payload.body)
			var foundObjects=$filter('filter')(vm.messagesRecieved,{id:message.id});
			if(foundObjects.length>0){
				foundObjects[0].content = message.content;
				foundObjects[0].edited = true;
			} else {
				vm.messagesRecieved.push(JSON.parse(payload.body));
			}
			$scope.$apply();
		}
		
		vm.addMessage = function(){
			var message = {id : ConnectionConfig.username+"."+Date.now()+"."+vm.message.to, from:ConnectionConfig.username, content: vm.message.content,to: vm.message.to};
			vm.messagesSent.push(message);
			vm.sendMessage(message)
		}
		
		vm.sendMessage = function(message){
			ConnectionConfig.stompClient.send("/app/chat.sendMessage", {}, 
					JSON.stringify(message));
		}
		
		vm.editMessage = function(message, event) {
			var confirm = $mdDialog.prompt()
				.title('Edit Message')
				.textContent('Change the message to resend it.')
				.placeholder('Message')
				.initialValue(message.content)
				.targetEvent(event)
				.ok('Send!')
				.cancel('Cancel');

			$mdDialog.show(confirm).then(
					function(result) {
						message.content = result;
						message.edited = true;
						vm.sendMessage(message);
					});
		}
		
	}
	
})();