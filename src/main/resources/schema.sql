DROP TABLE IF EXISTS chat_message;
CREATE TABLE chat_message (
  id          INTEGER PRIMARY KEY AUTO_INCREMENT,
  sender      VARCHAR(255) NOT NULL,
  to          VARCHAR(255) NOT NULL,
  delivered   BOOLEAN(1)   NOT NULL,
  client_id   VARCHAR(255) NOT NULL,
  message     VARCHAR(255) NOT NULL);