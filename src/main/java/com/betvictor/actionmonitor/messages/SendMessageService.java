package com.betvictor.actionmonitor.messages;

/**
 * 
 * @author Antonio
 *
 * Service interface that define how we send messages in different cases.
 *
 */
public interface SendMessageService {

	/**
	 * Send a new Message with the information contained in the MessageDTO
	 * @param message MessageDTO that contains all the necessary information for a message to be sent.
	 */
	public void sendNewMessage(MessageDTO message);
	
	
	/**
	 * Send non received messages to the user with the username given by username param.
	 * @param username User to whom we want to send the messages.
	 * The method will throw an IllegalArgumentException if the username is null or blank.
	 */
	public void retryPendingMessagesFor(String username);
	
}
