package com.betvictor.actionmonitor.messages;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
/**
 * 
 * @author Antonio
 *
 * This class will be the entity used for the DB interaction to save Chat messages.
 *
 */
@Entity(name="ChatMessage")
@Table(name="chat_message")
public class ChatMessageEntity {

	@Id
	@GeneratedValue
	private Integer id;
	
	private String sender;
	private String to;
	private String message;
	private Boolean delivered;
	/*
	 * id generated on the client side of the application that will allow the client to distinguish
	 * between different messages.
	 */
	private String clientId;
	
	public ChatMessageEntity(){
		super();
	}
	
	public ChatMessageEntity(Integer id, String sender, String to, String message, Boolean delivered, String clientId) {
		super();
		this.id = id;
		this.sender = sender;
		this.to = to;
		this.message = message;
		this.delivered = delivered;
		this.clientId = clientId;
	}
	
	/**
	 * This constructor will create a ChatMessage entity from the Message sent by the user.
	 * The constructor will map the information needed to the entity instance.
	 * 
	 * @param message The message sent to be inserted from the user
	 */
	public ChatMessageEntity(MessageDTO message){
		super();
		this.setMessage(message.getContent());
		this.setSender(message.getFrom());
		this.setTo(message.getTo());
		this.setDelivered(message.getDelivered());
		this.setClientId(message.getId());
	}
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getSender() {
		return sender;
	}
	public void setSender(String sender) {
		this.sender = sender;
	}
	public String getTo() {
		return to;
	}
	public void setTo(String to) {
		this.to = to;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}

	public Boolean getDelivered() {
		return delivered;
	}

	public void setDelivered(Boolean delivered) {
		this.delivered = delivered;
	}

	public String getClientId() {
		return clientId;
	}

	public void setClientId(String clientId) {
		this.clientId = clientId;
	}

	@Override
	public String toString() {
		return "ChatMessageEntity [id=" + id + ", sender=" + sender + ", to=" + to + ", message=" + message + ", delivered="
				+ delivered + ", clientId=" + clientId + "]";
	}

	
	/**
	 * This method will return MessageDTO instance created from a DB record.
	 * @return MessageDTO MessageDTO instance created from the DB record.
	 */
	public MessageDTO toMessage(){
		MessageDTO m = new MessageDTO();
		m.setContent(this.getMessage());
		m.setFrom(this.getSender());
		m.setTo(this.getTo());
		m.setDelivered(this.getDelivered());
		m.setId(this.getClientId());
		return m;
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((clientId == null) ? 0 : clientId.hashCode());
		result = prime * result + ((delivered == null) ? 0 : delivered.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((message == null) ? 0 : message.hashCode());
		result = prime * result + ((sender == null) ? 0 : sender.hashCode());
		result = prime * result + ((to == null) ? 0 : to.hashCode());
		return result;
	}



	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ChatMessageEntity other = (ChatMessageEntity) obj;
		if (clientId == null) {
			if (other.clientId != null)
				return false;
		} else if (!clientId.equals(other.clientId))
			return false;
		if (delivered == null) {
			if (other.delivered != null)
				return false;
		} else if (!delivered.equals(other.delivered))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (message == null) {
			if (other.message != null)
				return false;
		} else if (!message.equals(other.message))
			return false;
		if (sender == null) {
			if (other.sender != null)
				return false;
		} else if (!sender.equals(other.sender))
			return false;
		if (to == null) {
			if (other.to != null)
				return false;
		} else if (!to.equals(other.to))
			return false;
		return true;
	}
	
}
