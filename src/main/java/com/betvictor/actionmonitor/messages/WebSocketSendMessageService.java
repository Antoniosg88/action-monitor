package com.betvictor.actionmonitor.messages;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.inject.Inject;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;

import com.betvictor.actionmonitor.WebSocketsEnvConfig;


@Service
public class WebSocketSendMessageService implements SendMessageService {

	private Map<String,String> connectedUsers;
	private JmsTemplate jmsTemplate;
	private ChatMessageRepository messageRepo;
	private SimpMessagingTemplate simpMessagingTemplate;
	private WebSocketsEnvConfig config;
	
	private static final Logger logger = LoggerFactory.getLogger(WebSocketSendMessageService.class);

	@Inject
	public WebSocketSendMessageService(Map<String,String> connectedUsers, JmsTemplate jmsTemplate,
			ChatMessageRepository messageRepo, SimpMessagingTemplate simpMessagingTemplate,
			WebSocketsEnvConfig config){
		this.connectedUsers = connectedUsers;
		this.jmsTemplate = jmsTemplate;
		this.messageRepo = messageRepo;
		this.simpMessagingTemplate = simpMessagingTemplate;
		this.config = config;
	}
	
	@Override
	public void sendNewMessage(MessageDTO message) {
		
		if(message == null || StringUtils.isEmpty(message.getFrom()) || StringUtils.isEmpty(message.getTo())){
			throw new IllegalArgumentException("A messages must be passed with the proper format");
		}
		
		
		logger.info("Sending message: {}",message);
		Boolean delivered = false;
		if(connectedUsers.containsValue(message.getTo())){
			logger.info("Message will be sent to the connected user : {}",message.getTo());
			simpMessagingTemplate.convertAndSend(config.getBrokerChannel()+"/"+message.getTo(),message);
			delivered = true;
		}
		message.setDelivered(delivered);
		jmsTemplate.convertAndSend("saveMessage", message);
		simpMessagingTemplate.convertAndSend(config.getBrokerChannel()+"/action-monitor",message);
		
	}


	@Override
	public void retryPendingMessagesFor(String username) {
		logger.info("Resending unread messages to user : {}",username);
		if(StringUtils.isBlank(username)){
			throw new IllegalArgumentException("The username cannot be null nor blank");
		}
		List<ChatMessageEntity> messagesEntities = messageRepo.findByToAndDeliveredFalseGroupByClientId(username);
		List<MessageDTO> messages = messagesEntities.stream().map(ChatMessageEntity::toMessage).collect(Collectors.toList());
		messages.stream()
			.forEach(message ->{ 
				simpMessagingTemplate.convertAndSend(config.getBrokerChannel()+"/"+username,message);
				simpMessagingTemplate.convertAndSend(config.getBrokerChannel()+"/action-monitor",message.delivered());
			});
		if(!messagesEntities.isEmpty()){
			jmsTemplate.convertAndSend("updateDeliveredFlags",username);
		}
		
	}
	
	

}
