package com.betvictor.actionmonitor.messages;

import java.util.Map;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

/**
 * 
 * @author Antonio
 *
 * This class will be handling all the event that are sent in order to insert/update 
 * chat messages table on the database
 *
 */
@Component
public class ChatMessageQueueReceiver {

	private static final Logger logger = LoggerFactory.getLogger(ChatMessageQueueReceiver.class);

	
	private ChatMessageRepository chatMessageRepo;

	@Inject
	public ChatMessageQueueReceiver(ChatMessageRepository chatMessageRepo){
		this.chatMessageRepo = chatMessageRepo;
	}
	
	@JmsListener( destination ="saveMessage")
	public void receiveMessage(MessageDTO message){
		logger.info("Message received...{}",message);
		ChatMessageEntity entity = new ChatMessageEntity(message);
		chatMessageRepo.save(entity);
		logger.info("Message saved...{}",message);
	}
	
	@JmsListener( destination ="updateDeliveredFlags")
	public void updateDeliveredFlagForUser(String user){
		logger.info("Updating messages for...{}",user);
		chatMessageRepo.updateToDeliveredMessagesTo(user);
		logger.info("Updated messages for...{}",user);
	}
	
}
