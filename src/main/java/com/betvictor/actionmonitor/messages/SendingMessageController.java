package com.betvictor.actionmonitor.messages;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SendingMessageController {

	
	private static final Logger logger = LoggerFactory.getLogger(SendingMessageController.class);

	private SendMessageService messageService;
	
	@Inject
	public SendingMessageController(SendMessageService messageService){
		this.messageService = messageService;
	}
	
	@MessageMapping("/chat.sendMessage")
	public void sendMessage(@Payload MessageDTO message){
		logger.info("Message {}",message);
		messageService.sendNewMessage(message);
		
	}
	
	
}
