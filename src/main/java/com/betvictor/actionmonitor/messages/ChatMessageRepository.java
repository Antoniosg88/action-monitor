package com.betvictor.actionmonitor.messages;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

public interface ChatMessageRepository extends JpaRepository<ChatMessageEntity, Integer> {

	
	@Query("SELECT "
			+ "actives FROM ChatMessage actives "
			+ "WHERE actives.id= (SELECT MAX(others.id) FROM ChatMessage others WHERE others.clientId = actives.clientId)"
			+ "AND actives.delivered = FALSE AND actives.to = :to")
	public List<ChatMessageEntity> findByToAndDeliveredFalseGroupByClientId(@Param("to") String to);

	@Modifying
	@Transactional
	@Query("UPDATE ChatMessage message SET message.delivered=1 WHERE message.to=:to "
			+ "AND message.id = (SELECT MAX(others.id) FROM ChatMessage others WHERE others.clientId = message.clientId)")
	public int updateToDeliveredMessagesTo(@Param("to") String to);
	
}
