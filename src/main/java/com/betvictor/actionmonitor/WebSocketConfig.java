package com.betvictor.actionmonitor;

import javax.inject.Inject;

import org.springframework.context.annotation.Configuration;
import org.springframework.messaging.simp.config.MessageBrokerRegistry;
import org.springframework.web.socket.config.annotation.AbstractWebSocketMessageBrokerConfigurer;
import org.springframework.web.socket.config.annotation.EnableWebSocketMessageBroker;
import org.springframework.web.socket.config.annotation.StompEndpointRegistry;

@Configuration
@EnableWebSocketMessageBroker
public class WebSocketConfig extends AbstractWebSocketMessageBrokerConfigurer {

	private WebSocketsEnvConfig envConfig;
	
	@Inject
	public WebSocketConfig(WebSocketsEnvConfig envConfig) {
		this.envConfig = envConfig;
	}
	
	@Override
	public void registerStompEndpoints(StompEndpointRegistry registry) {
		registry.addEndpoint(envConfig.getStompEndpoint()).withSockJS();
	}
	
	@Override
	public void configureMessageBroker(MessageBrokerRegistry registry) {
		registry.setApplicationDestinationPrefixes(envConfig.getApplicationDestionationPrefix());
		registry.enableSimpleBroker(envConfig.getBrokerChannel());
	}

}
