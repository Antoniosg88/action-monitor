package com.betvictor.actionmonitor;

import java.util.Map;

import javax.inject.Inject;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.event.EventListener;
import org.springframework.messaging.simp.stomp.StompHeaderAccessor;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.messaging.SessionDisconnectEvent;
import org.springframework.web.socket.messaging.SessionSubscribeEvent;

import com.betvictor.actionmonitor.messages.SendMessageService;

@Component
public class WebSocketEventListener {

	private static final Logger logger = LoggerFactory.getLogger(WebSocketEventListener.class);
	
	/*
	 * We will keep in memory the connected users, in order to know which messages
	 * were sent with the receiver listening to them.
	 */
	private Map<String,String> connectedUsers;
	private SendMessageService messageService;
	private WebSocketsEnvConfig config;
	private StompHeaderAccessorWrapper stompHeaderAccessorWrapper;
	
	@Inject
	public WebSocketEventListener(Map<String,String> connectedUsers, SendMessageService messageService,
			WebSocketsEnvConfig config,StompHeaderAccessorWrapper stompHeaderAccessorWrapper) {
		this.connectedUsers = connectedUsers;
		this.messageService = messageService;
		this.config = config;
		this.stompHeaderAccessorWrapper = stompHeaderAccessorWrapper;
	}

	
	@EventListener
	public void handleSubscription(SessionSubscribeEvent event){
		logger.info("Receiving Subscription connection");
		logger.info("Raw event : {}",event);
		StompHeaderAccessor accessor = stompHeaderAccessorWrapper.wrap(event.getMessage());
		String username = accessor.getDestination().replaceAll(config.getBrokerChannel(), "");
		//We will check that we dont have an empty string with no user nor "/" wihtouth specifying the user
		if(StringUtils.isNotBlank(username) && username.length()>1){
			/*
			 * We add the new user logged, linked to the session for the websocket.
			 */
			username = username.replaceAll("/", "");
			connectedUsers.put(accessor.getSessionId(),username);
			messageService.retryPendingMessagesFor(username);
		}
		
		
	}
	
	@EventListener
	public void handleWebSocketDisconnect(SessionDisconnectEvent event){
		logger.info("Disconnection Event :{}",event);
		StompHeaderAccessor headerAccessor = stompHeaderAccessorWrapper.wrap(event.getMessage());
		
		/*
		 * If this condition is match, something has to be wrong on the actual state
		 * of the system, since the user should be found among the connected users in order
		 * to be able to disconnect form the system.
		 */
		String username = null;
		if((username = connectedUsers.remove(headerAccessor.getSessionId()))== null){
			logger.error("Error disconnecting an user");
		}
		
		logger.info("Disconnected : {}",username);
	}
	
}
