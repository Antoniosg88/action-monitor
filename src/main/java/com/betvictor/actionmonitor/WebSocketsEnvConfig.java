package com.betvictor.actionmonitor;

import javax.validation.constraints.NotNull;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@ConfigurationProperties(prefix="websockets")
@Configuration
public class WebSocketsEnvConfig {

	@NotNull
	private String stompEndpoint;
	
	@NotNull
	private String applicationDestionationPrefix;
	
	@NotNull
	private String brokerChannel;

	public String getStompEndpoint() {
		return stompEndpoint;
	}

	public String getApplicationDestionationPrefix() {
		return applicationDestionationPrefix;
	}

	public String getBrokerChannel() {
		return brokerChannel;
	}

	public void setStompEndpoint(String stompEndpoint) {
		this.stompEndpoint = stompEndpoint;
	}

	public void setApplicationDestionationPrefix(String applicationDestionationPrefix) {
		this.applicationDestionationPrefix = applicationDestionationPrefix;
	}

	public void setBrokerChannel(String brokerChannel) {
		this.brokerChannel = brokerChannel;
	}
	
	
}
