
INSERT INTO chat_message (id,sender,to,delivered,client_id,message) 
VALUES (1,'Antonio', 'Tiago',FALSE,'SomeJsId','Hi there');

INSERT INTO chat_message (id,sender,to,delivered,client_id,message) 
VALUES (2,'Antonio', 'Tiago',FALSE,'SomeJsId','Message Edited');

INSERT INTO chat_message (id,sender,to,delivered,client_id,message) 
VALUES (3,'Antonio', 'Tiago',FALSE,'OtherClientId','Different message');

INSERT INTO chat_message (id,sender,to,delivered,client_id,message) 
VALUES (4,'Antonio', 'Francisco',TRUE,'clientIdentifier','Greetings');

INSERT INTO chat_message (id,sender,to,delivered,client_id,message) 
VALUES (5,'Antonio', 'Francisco',TRUE,'messageNotDeliveredIDAfterUpdate','Hi there');

INSERT INTO chat_message (id,sender,to,delivered,client_id,message) 
VALUES (6,'Antonio', 'Francisco',FALSE,'messageNotDeliveredIDAfterUpdate','Hi!!');
