package com.betvictor.actionmonitor;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.HashMap;
import java.util.Map;

import org.hamcrest.collection.IsMapContaining;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.messaging.Message;
import org.springframework.messaging.simp.stomp.StompHeaderAccessor;
import org.springframework.web.socket.messaging.SessionDisconnectEvent;
import org.springframework.web.socket.messaging.SessionSubscribeEvent;

import com.betvictor.actionmonitor.messages.SendMessageService;

@RunWith(MockitoJUnitRunner.class)
public class WebSocketsEventListenerTest {

	private WebSocketEventListener webSocketEventListener;
	
	@Mock
	private SendMessageService messageService;
	
	@Mock
	private Message<byte[]> message;
	
	
	private WebSocketsEnvConfig config;
	
	@Mock
	private StompHeaderAccessorWrapper stompHeaderAccessorWrapper;

	@Mock 
	private StompHeaderAccessor mockStompHeaderAccessor;
	
	private Map<String,String> connectedUsers;
	
	@Before
	public void setUp(){
		connectedUsers = new HashMap<>();
		config = new WebSocketsEnvConfig();
		config.setBrokerChannel("/channel");
		webSocketEventListener = new WebSocketEventListener(connectedUsers,messageService,config,stompHeaderAccessorWrapper);
	}
	
	@Test
	public void weShouldNotHaveAnyConnectedUserIfUserWasNotSendToTheSubcribeEvent(){
		SessionSubscribeEvent event = Mockito.mock(SessionSubscribeEvent.class);
		when(event.getMessage()).thenReturn(message);
		when(stompHeaderAccessorWrapper.wrap(message)).thenReturn(mockStompHeaderAccessor);
		when(mockStompHeaderAccessor.getDestination()).thenReturn(config.getBrokerChannel());
		webSocketEventListener.handleSubscription(event);
		
		assertThat(connectedUsers.size(),equalTo(0));
	}
	
	@Test
	public void weShouldNotHaveAnyConnectedUserIfUserWasNotSendToTheSubcribeEventButTheSlash(){
		SessionSubscribeEvent event = Mockito.mock(SessionSubscribeEvent.class);
		when(event.getMessage()).thenReturn(message);
		when(stompHeaderAccessorWrapper.wrap(message)).thenReturn(mockStompHeaderAccessor);
		when(mockStompHeaderAccessor.getDestination()).thenReturn(config.getBrokerChannel()+"/");
		webSocketEventListener.handleSubscription(event);
		
		assertThat(connectedUsers.size(),equalTo(0));
	}
	
	@Test
	public void weShouldHaveAConnectedUserIfUserWasSentToTheSubcribeEvent(){
		SessionSubscribeEvent event = Mockito.mock(SessionSubscribeEvent.class);
		when(event.getMessage()).thenReturn(message);
		when(stompHeaderAccessorWrapper.wrap(message)).thenReturn(mockStompHeaderAccessor);
		when(mockStompHeaderAccessor.getDestination()).thenReturn(config.getBrokerChannel()+"/fakeUser");
		when(mockStompHeaderAccessor.getSessionId()).thenReturn("aSessionId");
		webSocketEventListener.handleSubscription(event);
		
		assertThat(connectedUsers.size(),equalTo(1));
		assertThat(connectedUsers, IsMapContaining.hasValue("fakeUser"));
		verify(messageService,times(1)).retryPendingMessagesFor("fakeUser");
		
	}
	
	@Test
	public void weShouldHaveSameConnectedUserIfErrorDisconnecting(){
		SessionDisconnectEvent event = Mockito.mock(SessionDisconnectEvent.class);
		connectedUsers.put("aDifferentSession", "Arya");
		when(event.getMessage()).thenReturn(message);
		when(stompHeaderAccessorWrapper.wrap(message)).thenReturn(mockStompHeaderAccessor);
		when(mockStompHeaderAccessor.getSessionId()).thenReturn("aSessionId");
		webSocketEventListener.handleWebSocketDisconnect(event);
		
		assertThat(connectedUsers.size(),equalTo(1));
		assertThat(connectedUsers, IsMapContaining.hasValue("Arya"));
		
	}
	
	@Test
	public void weShouldHaveOneLessConnectedUserIfSuccessDisconnecting(){
		SessionDisconnectEvent event = Mockito.mock(SessionDisconnectEvent.class);
		connectedUsers.put("aSessionId", "Arya");
		connectedUsers.put("anotherSessionId", "asdf");
		when(event.getMessage()).thenReturn(message);
		when(stompHeaderAccessorWrapper.wrap(message)).thenReturn(mockStompHeaderAccessor);
		when(mockStompHeaderAccessor.getSessionId()).thenReturn("aSessionId");
		webSocketEventListener.handleWebSocketDisconnect(event);
		
		assertThat(connectedUsers.size(),equalTo(1));
		assertThat(connectedUsers, IsMapContaining.hasValue("asdf"));
		
	}
	
	
}
