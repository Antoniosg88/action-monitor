package com.betvictor.actionmonitor.messages;


import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;

import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

import javax.inject.Inject;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import com.betvictor.actionmonitor.messages.ChatMessageEntity;
import com.betvictor.actionmonitor.messages.ChatMessageRepository;


@RunWith(SpringRunner.class)
@SpringBootTest()
public class ChatMessageRepositoryTest {

	@Inject
	private ChatMessageRepository repo;
	
	
	@Test
	public void weShouldOnlyGetMessagesWithSameClientIdAndGreaterIdWhenNotDelivered(){
		List<ChatMessageEntity> messages = repo.findByToAndDeliveredFalseGroupByClientId("Tiago");
		
		assertThat(messages.size(), equalTo(2));
		Map<String,ChatMessageEntity> entitiesMap = messages.stream().collect(Collectors.toMap(ChatMessageEntity::getClientId, Function.identity()));
		assertThat(true, equalTo(entitiesMap.containsKey(messages.get(0).getClientId())));
		assertThat(true, equalTo(entitiesMap.containsKey(messages.get(1).getClientId())));
		
		assertThat(entitiesMap.get("SomeJsId").getMessage(),equalTo("Message Edited"));
	}
	
	@Test
	public void weShouldGetOnlyNotDeliveredMessageWhenFirstOneWithSameIdWasDevivered(){
		List<ChatMessageEntity> messages = repo.findByToAndDeliveredFalseGroupByClientId("Francisco");
		
		assertThat(messages.size(), equalTo(1));
		assertThat("messageNotDeliveredIDAfterUpdate", equalTo(messages.get(0).getClientId()));
		assertThat(6,equalTo(messages.get(0).getId()));
		assertThat("Hi!!",equalTo(messages.get(0).getMessage()));
	}
	
	@Test
	@Transactional
	public void weShouldOnlyUpdateTheTwoRecordsWeAreDeliveringNotTheOneWeCouldNotSentAtAll(){
		int updated = repo.updateToDeliveredMessagesTo("Tiago");
		List<ChatMessageEntity> entities = repo.findAll();
		List<ChatMessageEntity> filteredEntity = entities.stream().filter(element-> !element.getDelivered() && element.getTo().equals("Tiago")).collect(Collectors.toList());
		
		assertThat(updated,equalTo(2));
		assertThat(filteredEntity.size(),equalTo(1));
		assertThat(filteredEntity.get(0).getId(),equalTo(1));
		
	}
	
	
}
