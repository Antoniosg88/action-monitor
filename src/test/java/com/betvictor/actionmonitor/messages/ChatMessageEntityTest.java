package com.betvictor.actionmonitor.messages;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.junit.Assert.assertThat;

import org.junit.Test;

import com.betvictor.actionmonitor.messages.ChatMessageEntity;
import com.betvictor.actionmonitor.messages.MessageDTO;

public class ChatMessageEntityTest {

	@Test
	public void weShouldCreateEntityFromMessageDTOWithTheValuesContainedOnMessageDTO(){
		MessageDTO dto = new MessageDTO("from","content","to",false,false,"clientId");
		
		ChatMessageEntity entity = new ChatMessageEntity(dto);
		
		assertThat(entity.getClientId(),equalTo("clientId"));
		assertThat(entity.getId(),nullValue());
		assertThat(entity.getMessage(),equalTo("content"));
		assertThat(entity.getSender(),equalTo("from"));
		assertThat(entity.getTo(),equalTo("to"));
	}
	
	@Test
	public void weShouldCreateAMessageDTOFromEntityWithAllPossibleValuesContainedOnTheEntity(){
		ChatMessageEntity entity = new ChatMessageEntity(1, "from", "to", "A message", true, "clientId");
		
		MessageDTO dto = entity.toMessage();
		
		assertThat(dto.getContent(),equalTo("A message"));
		assertThat(dto.getDelivered(),equalTo(true));
		assertThat(dto.getEdited(),equalTo(null));
		assertThat(dto.getFrom(),equalTo("from"));
		assertThat(dto.getId(),equalTo("clientId"));
		assertThat(dto.getTo(),equalTo("to"));
		
	}
	
}
