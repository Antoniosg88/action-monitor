package com.betvictor.actionmonitor.messages;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.betvictor.actionmonitor.messages.ChatMessageEntity;
import com.betvictor.actionmonitor.messages.ChatMessageQueueReceiver;
import com.betvictor.actionmonitor.messages.ChatMessageRepository;
import com.betvictor.actionmonitor.messages.MessageDTO;

@RunWith(MockitoJUnitRunner.class)
public class ChatMessageQueueReceiverTest {

	
	private ChatMessageQueueReceiver queue;
	
	@Mock
	private ChatMessageRepository repo;
	
	@Before
	public void setUp(){
		queue = new ChatMessageQueueReceiver(repo);
	}
	
	@Test
	public void weShouldSaveToTheDBTheInformationSentInTheMessage(){
		MessageDTO dto = new MessageDTO("from", "content", "to", true, false, "aJsId");
		queue.receiveMessage(dto);
		ChatMessageEntity entity = new ChatMessageEntity(null, "from", "to", "content", true, "aJsId");
		verify(repo,times(1)).save(entity);
	}
	
	@Test
	public void weShouldUpdateDeliveredFlagForUserGiven(){
		queue.updateDeliveredFlagForUser("anUser");
		verify(repo,times(1)).updateToDeliveredMessagesTo("anUser");
	}
	
}
