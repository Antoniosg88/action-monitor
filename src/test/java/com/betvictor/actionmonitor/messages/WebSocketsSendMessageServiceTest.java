package com.betvictor.actionmonitor.messages;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.messaging.simp.SimpMessagingTemplate;

import com.betvictor.actionmonitor.WebSocketsEnvConfig;
import com.betvictor.actionmonitor.messages.ChatMessageEntity;
import com.betvictor.actionmonitor.messages.ChatMessageRepository;
import com.betvictor.actionmonitor.messages.MessageDTO;
import com.betvictor.actionmonitor.messages.WebSocketSendMessageService;

@RunWith(MockitoJUnitRunner.class)
public class WebSocketsSendMessageServiceTest {

	private WebSocketSendMessageService sendMessageService;
	
	private Map<String,String> connectedUsers;
	
	@Mock
	private ChatMessageRepository repo;
	
	@Mock
	private JmsTemplate jmsTemplate;
	
	@Mock
	private SimpMessagingTemplate simpMessagingTemplate;
	
	@Mock
	private WebSocketsEnvConfig config;
	
	@Before
	public void setUp(){
		connectedUsers = new HashMap<>();
		connectedUsers.put("key", "connected");
		when(config.getBrokerChannel()).thenReturn("/channel");
		sendMessageService = new WebSocketSendMessageService(connectedUsers, jmsTemplate, repo, simpMessagingTemplate,config);
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void sendingNullShouldRaiseAnIllegalArgumentException(){
		sendMessageService.retryPendingMessagesFor(null);
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void sendingEmptyShouldRaiseAnIllegalArgumentException(){
		sendMessageService.retryPendingMessagesFor("");
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void sendingBlankShouldRaiseAnIllegalArgumentException(){
		sendMessageService.retryPendingMessagesFor(" ");
	}
	
	@Test()
	public void weShouldTakeNoActionIfThereIsNoPendingMessges(){
		when(repo.findByToAndDeliveredFalseGroupByClientId("unsername")).thenReturn(new ArrayList<>());
		sendMessageService.retryPendingMessagesFor("username");
		verify(simpMessagingTemplate,times(0)).convertAndSend(any(String.class),any(MessageDTO.class));
		verify(jmsTemplate, times(0)).convertAndSend("updateSentFlags", "username");
	}
	
	
	@Test()
	public void weShouldSendMessagesToUserChannelAndActionMonitorAsWellAsAMessageToTheQueueToUpdateSentFlags(){
		ChatMessageEntity entity = new ChatMessageEntity(1, "from", "to", "message", false, "clientId");
		List<ChatMessageEntity> entities = Arrays.asList(entity);
		when(repo.findByToAndDeliveredFalseGroupByClientId("username")).thenReturn(entities);
		sendMessageService.retryPendingMessagesFor("username");
		MessageDTO dtoExpectedToUserChannel = new MessageDTO("from","message","to",true,null,"clientId");
		MessageDTO dtoExpectedToActionMonitorChannel = new MessageDTO("from","message","to",true,null,"clientId");
		verify(simpMessagingTemplate,times(1)).convertAndSend(eq("/channel/username"),eq(dtoExpectedToUserChannel));
		verify(simpMessagingTemplate,times(1)).convertAndSend("/channel/action-monitor",dtoExpectedToActionMonitorChannel);
		verify(jmsTemplate, times(1)).convertAndSend("updateDeliveredFlags", "username");
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void sendingNullShouldRaiseAnIllegalArgumentExceptionWhenSendingNewMessage(){
		sendMessageService.sendNewMessage(null);
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void sendingNullShouldRaiseAnIllegalArgumentExceptionWhenMessageDoesNotHaveAnOrigin(){
		MessageDTO dto = new MessageDTO();
		sendMessageService.sendNewMessage(dto);
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void sendingNullShouldRaiseAnIllegalArgumentExceptionWhenMessageDoesNotHaveADestination(){
		MessageDTO dto = new MessageDTO();
		dto.setFrom("from");
		sendMessageService.sendNewMessage(dto);
	}
	
	@Test
	public void sendingAMessageToANonConnectedUserShouldNotSentAMessageToTheUserChannelAndSendTheMessageToTheQueueAsNotSent(){
		MessageDTO dto = new MessageDTO();
		dto.setFrom("from");
		dto.setTo("to");
		sendMessageService.sendNewMessage(dto);
		MessageDTO expectedDTOOToJmsAndWebSocket = new MessageDTO();
		expectedDTOOToJmsAndWebSocket.setFrom("from");
		expectedDTOOToJmsAndWebSocket.setTo("to");
		expectedDTOOToJmsAndWebSocket.setDelivered(false);
		verify(jmsTemplate, times(1)).convertAndSend("saveMessage",expectedDTOOToJmsAndWebSocket);
		verify(simpMessagingTemplate, times(1)).convertAndSend("/channel/action-monitor",expectedDTOOToJmsAndWebSocket);
		verify(simpMessagingTemplate, times(0)).convertAndSend(eq("/channel/to"),any(MessageDTO.class));
	}
	
	
	@Test
	public void weShouldSendMessagesToTheActionMonitorAndUserWebSocketAndTotheJMSQueueInOrderToInsertTheMessages(){
		MessageDTO dto = new MessageDTO();
		dto.setFrom("from");
		dto.setTo("connected");
		sendMessageService.sendNewMessage(dto);
		MessageDTO expectedDTOOToJmsAndWebSocket = new MessageDTO();
		expectedDTOOToJmsAndWebSocket.setFrom("from");
		expectedDTOOToJmsAndWebSocket.setTo("connected");
		expectedDTOOToJmsAndWebSocket.setDelivered(true);
		verify(jmsTemplate, times(1)).convertAndSend("saveMessage",expectedDTOOToJmsAndWebSocket);
		verify(simpMessagingTemplate, times(1)).convertAndSend("/channel/action-monitor",expectedDTOOToJmsAndWebSocket);
		verify(simpMessagingTemplate, times(1)).convertAndSend("/channel/connected",dto);
	}
	
}
