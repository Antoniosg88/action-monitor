package com.betvictor.actionmonitor.messages;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class MessageDTOTest {

	
	@Test
	public void weShouldSetTheRightValuesToDifferentAttrsOnFieldConstructor(){
		MessageDTO dto = new MessageDTO("from", "content", "to", false, false, "id");
		
		assertThat(dto.getContent(),equalTo("content"));
		assertThat(dto.getDelivered(),equalTo(false));
		assertThat(dto.getEdited(),equalTo(false));
		assertThat(dto.getFrom(),equalTo("from"));
		assertThat(dto.getId(),equalTo("id"));
		assertThat(dto.getTo(),equalTo("to"));
	}
	
	@Test
	public void weShouldUpdateDeliveredToTrueToTheObjectCallingTheMethod(){
		MessageDTO dto = new MessageDTO("from", "content", "to", false, false, "id");
		assertThat(dto.getDelivered(),equalTo(false));
		MessageDTO dtoGenerated = dto.delivered();
		assertThat(dtoGenerated.getDelivered(),equalTo(true));
		//We check that the instance is the same, despite a MessageDTO is returned on the delivered method
		//We are checking behavior in which we trust, and don't want to be modified
		assertTrue(dtoGenerated==dto);
		
	}

}
