package com.betvictor.actionmonitor.messages;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.betvictor.actionmonitor.messages.MessageDTO;
import com.betvictor.actionmonitor.messages.SendMessageService;
import com.betvictor.actionmonitor.messages.SendingMessageController;

@RunWith(MockitoJUnitRunner.class)
public class SendingMessagesControllerTest {

	private SendingMessageController controller;
	
	@Mock
	private SendMessageService service;
	
	@Before
	public void setUp(){
		controller = new SendingMessageController(service);
	}
	
	@Test
	public void weShouldCallTheServiceToHandleTheActionOfSendingTheMessage(){
		MessageDTO dto = new MessageDTO("from", "content", "To", true, false, "aJsId");
		controller.sendMessage(dto);
		verify(service,times(1)).sendNewMessage(dto);
	}
	
}
